function letsRetro() {
  $(this).removeClass("reveal-btn");
  $(this).addClass("hide");
  $(".column-header").css("display", "block");
}

$(document).ready(function() {
  $(".reveal-btn").click(letsRetro);

  $(".celebrate-btn").click(function() {
    if($("#celebrate").css("display") ==="none") {
      $("#celebrate").css("display","block");
    } else {
      $("#celebrate").css("display","none");
    }
  });

  $(".seeking-btn").click(function() {
    if($("#seeking").css("display") ==="none") {
      $("#seeking").css("display","block");
    } else {
      $("#seeking").css("display","none");
    }
  });

  $(".growth-btn").click(function() {
    if($("#growth").css("display") ==="none") {
      $("#growth").css("display","block");
    } else {
      $("#growth").css("display","none");
    }
  });
});
