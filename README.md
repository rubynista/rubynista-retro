# Rubynista Retro #

This is my introduction to the folks at Basecamp.

### Technical Details ###

* What: Static Rack App
* UI Framework: [MaterializeCSS](http://materializecss.com)
* Level of fun: 100%
* Deployed: [Heroku](http://rubynista-retro.herokuapp.com)